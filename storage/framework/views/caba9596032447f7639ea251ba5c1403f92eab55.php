

<?php $__env->startSection('content'); ?>
<div id="mainContainer">
    <div class="container" id="con">
        <br/>
        <!-- <br/> -->
        <div class="row justify-content-center" >
            <div class="col-md-8">
                <div class="card">
                    <!-- <div class="card-header"><?php echo e(__('File Upload')); ?></div> -->

                    <div class="card-body">
                        <form method="POST" action="/teacher/<?php echo e($user->id); ?>" enctype="multipart/form-data" id="fileUpload">
                            <?php echo csrf_field(); ?>
                        
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right"><?php echo e(__('File')); ?></label>

                                <div class="col-md-6">
                                    <input id="" type="file" class="form-control <?php $__errorArgs = ['file'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="file" value="<?php echo e(old('file')); ?>" required  autofocus>

                                    <?php $__errorArgs = ['file'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="grade" class="col-md-4 col-form-label text-md-right"><?php echo e(__('For Grade')); ?></label>

                                <div class="col-md-6">
                                    <select class="form-control" name="grade">
                                        <?php if($user->class == "HighSchool"): ?>
                                            <option value="9">10</option>
                                            <option value="10">9</option>
                                        <?php endif; ?>
                                        <?php if($user->class == "Elementory"): ?>
                                            <option value="8">8</option>
                                            <option value="7">7</option>
                                        <?php endif; ?>
                                        <?php if($user->class == "Natural"): ?>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        <?php endif; ?>
                                        <?php if($user->class == "Social"): ?>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        <?php endif; ?>

                                    </select>
                                    
                                        
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="subject" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Subject')); ?></label>

                                <div class="col-md-6">
                                    <select class="form-control" name="subject">
                                        <?php if($user->class == "HighSchool" || $user->class == "Elementory"): ?>
                                            <option value="English">English</option>
                                            <option value="Amharic">Amharic</option>
                                            <option value="History">History</option>
                                            <option value="Geography">Geography</option>
                                            <option value="Civics">Civics</option>
                                            <option value="Physics">Physics</option>
                                            <option value="Maths">Maths</option>
                                            <option value="Chemistry">Chemistry</option>

                                        <?php endif; ?>
                                        
                                        <?php if($user->class == "Natural"): ?>
                                            <option value="English">English</option>
                                            <option value="Amharic">Amharic</option>
                                            
                                            <option value="Civics">Civics</option>
                                            <option value="Physics">Physics</option>
                                            <option value="Maths">Maths</option>
                                            <option value="Chemistry">Chemistry</option>
                                        <?php endif; ?>
                                        <?php if($user->class == "Social"): ?>
                                            <option value="English">English</option>
                                            <option value="Amharic">Amharic</option>
                                            <option value="History">History</option>
                                            <option value="Geography">Geography</option>
                                            <option value="Civics">Civics</option>
                                            
                                            <option value="Maths">Maths</option>
                                            
                                        <?php endif; ?>

                                    </select>
                                    
                                        
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Catagory" class="col-md-4 col-form-label text-md-right">Catagory</label>
                                
                                <select  name="Catagory" class="col-md-6 form-control">
                                    <option value="Video tutorial">Video Tutorial</option>
                                    <option value="tutorials">Tutorial</option>
                                    <option value="WorkSheets">WorkSheets</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Description')); ?></label>

                                <div class="col-md-6">
                                    <textarea name="description" class="form-control"></textarea>

                                    <!-- <?php $__errorArgs = ['file'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> -->
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        <?php echo e(__('Upload')); ?>

                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--      -->
<div>
    <div class="row justify-content-center" id="underContainer">
        <div class="col-md-4 rowDiv2">
           <div class="card">
                <div class="card-header">
                    Videos
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if($videosNum > 0): ?>
                            <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-11 rowDiv" >
                                    <div class="card">
                                        <div class="card-header"><img src="<?php echo e(asset('images/video.png')); ?>" width="100" height="100" class="center"></div>                    
                                    
                                        <div class="card-body" class="material">
                                            <b>Subject</b>-<?php echo e($video->subject); ?><br/>
                                            <b> Grade</b>-<?php echo e($video->grade); ?><br/>
                                            <b> Description</b>-<?php echo e($video->description); ?><br/>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <h3>You didnot upload any videos</h3>
                        <?php endif; ?>
                    </div>
                    <!-- <video src="<?php echo e(asset('Materials/Laravel 5.2 How to display image from database on laravel 5.2 part 8.mp4')); ?>" width="200" height="200">
                    </video> -->
                </div>
            </div>
        </div>
        <div class="col-md-4 rowDiv2">
           <div class="card">
                <div class="card-header">
                    Pdf
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if($pdfNum > 0): ?>
                            <?php $__currentLoopData = $pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pdf): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-11 rowDiv" >
                
                                    <div class="card">
                                        <div class="card-header" ><img src="<?php echo e(asset('images/pdf.png')); ?>" width="100" height="100" class="center"></div>                    
                                
                                    <div class="card-body" class="material">
                                        <b>Subject</b>-<?php echo e($pdf->subject); ?><br/>
                                        <b> Grade</b>-<?php echo e($pdf->grade); ?><br/>
                                        <b> Description</b>-<?php echo e($pdf->description); ?><br/>
                                        <a class="gradeLink" href="/student/view/pdf/<?php echo e($pdf->id); ?>">Open Pdf</a>
                                    </div>
                                </div>
                            
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <h3>You didnot upload any tutorials</h3>
                        <?php endif; ?>
                    </div>
                    <!-- <video src="<?php echo e(asset('Materials/Laravel 5.2 How to display image from database on laravel 5.2 part 8.mp4')); ?>" width="200" height="200">
                    </video> -->
                    <!-- <iframe src="<?php echo e(asset('Materials/1590765298.pdf')); ?>" width="100" height="100"></iframe>  -->
                </div>
            </div>
        </div>
        <div class="col-md-4 rowDiv2">

            <div class="card">
                <div class="card-header">
                    WorkSheets
                </div>
                <div class="card-body">
                    <?php if($sheetsNum > 0): ?>
                        <h3>there are worksheets</h3>
                    <?php else: ?>
                        <h3>You didnot upload any worksheets</h3>
                    <?php endif; ?>
                    <!-- <video src="<?php echo e(asset('Materials/Laravel 5.2 How to display image from database on laravel 5.2 part 8.mp4')); ?>" width="200" height="200">
                    </video> -->
                </div>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\hahu\resources\views/teacher.blade.php ENDPATH**/ ?>