

<?php $__env->startSection('content'); ?>

<div class="container">

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter no" class="form-control col-md-4">
     </div>

</div>
<div class="header" id="staffContainer" >
<div class="row">

   
    <?php if(count($notifiedMaterials) >= 1): ?>

        <?php $__currentLoopData = $notifiedMaterials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $material): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          
                <div class="card col-md-4">
                	<?php if($material->type == 'tutorials' ): ?>
                     <div class="card-header" ><img src="<?php echo e(asset('images/pdf.png')); ?>" width="100" height="100" class="center"></div>                    
                    <?php endif; ?>
                    <?php if($material->type == 'Video tutorial' ): ?>
                      <div class="card-header"><img src="<?php echo e(asset('images/video.png')); ?>" width="100" height="100" class="center"></div> 
                    <?php endif; ?>
                
                    <div class="card-body">
                    	<div >
                    	 Uploaded By:Teacher with id <?php echo e($material->teacherId); ?><br /><br />
                    	 <b>Subject</b>-<?php echo e($material->subject); ?><br/>
                         <b> Grade</b>-<?php echo e($material->grade); ?><br/>
                       <b> Description</b>-<?php echo e($material->description); ?><br/>
                       <?php if($material->type == 'tutorials' ): ?>
                          <a class="gradeLink2" href="/student/view/pdf/<?php echo e($material->id); ?>">Open <?php echo e($material->type); ?></a>
                          <a class="pull-right">Mark as Read</a>
                        <?php endif; ?>
                        <?php if($material->type == 'Video tutorial' ): ?>
                          <a class="gradeLink2" href="/student/view/video/<?php echo e($material->id); ?>">play</a>
                          <a class="pull-right" href="/notification/read/<?php echo e($material->notificationId); ?>">Mark as Read</a>
                        <?php endif; ?>

                        
                       
                    </div>  
                </div>
        <!-- </form> -->
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <h1 id="empty">You donot have  notification</h1>

    <?php endif; ?>
  </div>
 </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\hahu\resources\views/admin.blade.php ENDPATH**/ ?>