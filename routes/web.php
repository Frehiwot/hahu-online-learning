<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','HomeController@index');
Route::get('/chat', 'ChatsController@index');

//Route::get('/','ChatsController@index');
Auth::routes();
Route::get('messages','ChatsController@fetchMessages');
Route::post('messages','ChatsController@sendMessage');
Route::post('/admin/register','StudentController@register');
Route::get('/register/users','StudentController@view');
Route::get('teacher','TeacherController@index');
Route::post('teacher/{id}','TeacherController@upload');
Route::get('student','StudentController@index');
Route::get('student/grade/{id}','StudentController@Grade');
Route::get('student/grade/{id}/{subject}','StudentController@Subject');
Route::get('student/view/pdf/{id}','StudentController@viewPdf');
Route::get('student/view/video/{id}','StudentController@viewVideo');
Route::get('/admin/notifications','notificationController@get');
Route::get('/notification/read/{id}','notificationController@read');



