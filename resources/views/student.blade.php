@extends('layouts.app')

@section('content')
<!-- <div class="sec">
</div> -->
<div class="container">

<!-- <a href="" class="gradeLink">Grade 6</a>
<a href="" class="gradeLink">Grade 7</a>
<a href="" class="gradeLink">Grade 8</a>
<a href="" class="gradeLink">Grade 9</a>
<a href="" class="gradeLink">Grade 10</a>
<a href="" class="gradeLink">Grade 11</a>
<a href="" class="gradeLink">Grade 12</a> -->
<div class="dropdown">
  <a href="/student/grade/6" class="gradeLink">Grade 6</a>
  <div class="dropdown-content">
   
    <a href="English">English</a>
    <a href="Amharic">Amharic</a>
    <a href="History">History</a>
    <a href="Geography">Geography</a>
    <a href="Civics">Civics</a>
    <a href="Physics">Physics</a>
    <a href="Maths">Maths</a>
    <a href="Chemistry">Chemistry</a>
  </div>
</div>
<div class="dropdown">
  <a href="/student/grade/7" class="gradeLink">Grade 7</a>
  <div class="dropdown-content">
    
    <a href="/student/grade/7/English">English</a>
    <a href="/student/grade/7/Amharic">Amharic</a>
    <a href="/student/grade/7/History">History</a>
    <a href="/student/grade/7/Geography">Geography</a>
    <a href="/student/grade/7/Civics">Civics</a>
    <a href="/student/grade/7/Physics">Physics</a>
    <a href="/student/grade/7/Maths">Maths</a>
    <a href="/student/grade/7/Chemistry">Chemistry</a>
  </div>
</div>
<div class="dropdown">
  <a href="/student/grade/8" class="gradeLink">Grade 8</a>
  <div class="dropdown-content">
    
    <a href="/student/grade/8/English">English</a>
    <a href="/student/grade/8/Amharic">Amharic</a>
    <a href="/student/grade/8/History">History</a>
    <a href="/student/grade/8/Geography">Geography</a>
    <a href="/student/grade/8/Civics">Civics</a>
    <a href="/student/grade/8/Physics">Physics</a>
    <a href="/student/grade/8/Maths">Maths</a>
    <a href="/student/grade/8/Chemistry">Chemistry</a>
  </div>
</div>
<div class="dropdown">
  <a href="/student/grade/9" class="gradeLink">Grade 9</a>
  <div class="dropdown-content">
  <a href="/student/grade/9/English">English</a>
    <a href="/student/grade/9/Amharic">Amharic</a>
    <a href="/student/grade/9/History">History</a>
    <a href="/student/grade/9/Geography">Geography</a>
    <a href="/student/grade/9/Civics">Civics</a>
    <a href="/student/grade/9/Physics">Physics</a>
    <a href="/student/grade/9/Maths">Maths</a>
    <a href="/student/grade/9/Chemistry">Chemistry</a>
  </div>
</div>
<div class="dropdown">
  <a href="/student/grade/10" class="gradeLink">Grade 10</a>
  <div class="dropdown-content">
    <a href="/student/grade/10/English">English</a>
    <a href="/student/grade/10/Amharic">Amharic</a>
    <a href="/student/grade/10/History">History</a>
    <a href="/student/grade/10/Geography">Geography</a>
    <a href="/student/grade/10/Civics">Civics</a>
    <a href="/student/grade/10/Physics">Physics</a>
    <a href="/student/grade/10/Maths">Maths</a>
    <a href="/student/grade/10/Chemistry">Chemistry</a>
  </div>
</div>
<div class="dropdown">
  <a href="/student/grade/11" class="gradeLink">Grade 11</a>
  <div class="dropdown-content">
    
    <a href="/student/grade/11/English">English</a>
    <a href="/student/grade/11/Amharic">Amharic</a>
    
    <a href="/student/grade/11/Civics">Civics</a>
    <a href="/student/grade/11/Physics">Physics</a>
    <a href="/student/grade/11/Maths">Maths</a>
    <a href="/student/grade/11/Chemistry">Chemistry</a>
    <a href="/student/grade/11/History">History</a>
    <a href="/student/grade/11/Geography">Geography</a>
    <a href="/student/grade/11/Civics">Civics</a>
</div>
</div>
<div class="dropdown">
  <a href="/student/grade/12" class="gradeLink">Grade 12</a>
  <div class="dropdown-content">
    
    <a href="/student/grade/12/English">English</a>
    <a href="/student/grade/12/Amharic">Amharic</a>
    
    <a href="/student/grade/12/Civics">Civics</a>
    <a href="/student/grade/12/Physics">Physics</a>
    <a href="/student/grade/12/Maths">Maths</a>
    <a href="/student/grade/12/Chemistry">Chemistry</a>
    <a href="/student/grade/12/History">History</a>
    <a href="/student/grade/12/Geography">Geography</a>
    <a href="/student/grade/12/Civics">Civics</a>
  </div>
</div>
<div class="container" id="secondContainer">
    <div class="row">
    @if($length > 0)
        @foreach($materials as $material)
            @if($material->type == "Video tutorial")
                <div class="col-md-4 rowDiv" >
                    <div class="card">
                        <div class="card-header"><img src="{{ asset('images/video.png') }}" width="100" height="100" class="center"></div>                    
                    
                    <div class="card-body" class="material">
                        <b>Subject</b>-{{$material->subject}}<br/>
                       <b> Grade</b>-{{$material->grade}}<br/>
                       <b> Description</b>-{{$material->description}}<br/>
                       <div class="row justify-center">
                          <div class="col-md-11">
                             <a class="gradeLink2" href="/student/view/video/{{ $material->id }}">play</a>
                         </div>
                       </div>
                    </div>
                </div>
                </div>
            @endif
            @if($material->type == "tutorials")
                <div class="col-md-4 rowDiv" >
                
                    <div class="card">
                        <div class="card-header" ><img src="{{ asset('images/pdf.png') }}" width="100" height="100" class="center"></div>                    
                  
                    <div class="card-body" class="material">
                        <b>Subject</b>-{{$material->subject}}<br/>
                       <b> Grade</b>-{{$material->grade}}<br/>
                       <b> Description</b>-{{$material->description}}<br/>
                       <a class="gradeLink2" href="/student/view/pdf/{{ $material->id }}">Open Pdf</a>
                    </div>
                  </div>
                
                </div>
            @endif
            @if($material->type == "WorkSheets")
                <div class="col-md-4 rowDiv" >
                
                    <div class="card">
                        <div class="card-header"><img src="{{ asset('images/sheet.png') }}" width="100" height="100" class="center"></div>                    
                  
                    <div class="card-body" class="material">
                        <b>Subject</b>-{{$material->subject}}<br/>
                       <b> Grade</b>-{{$material->grade}}<br/>
                       <b> Description</b>-{{$material->description}}<br/>
                       <a class="gradeLink2" href="/student/view/pdf/{{ $material->id }}">Open Sheet</a>
                    </div>
                  </div>
               
                </div>
            @endif
        @endforeach
    @else
        <h3 id="noFile">No file found</h3>
    @endif
            
</div>
</div>



</div>

@endsection