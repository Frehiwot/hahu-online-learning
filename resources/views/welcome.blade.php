
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<title>HaHu &#8211; Learn from home</title>
<meta name='robots' content='noindex,follow' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="HaHu &raquo; Feed" href="http://localhost/wordpress/feed/" />
<link rel="alternate" type="application/rss+xml" title="HaHu &raquo; Comments Feed" href="http://localhost/wordpress/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/localhost\/wordpress\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<style id='astra-theme-css-inline-css' type='text/css'>
html{font-size:93.75%;}a,.page-title{color:#0274be;}a:hover,a:focus{color:#3a3a3a;}body,button,input,select,textarea{font-family:Times,Georgia,serif;font-weight:400;font-size:15px;font-size:1rem;line-height:3.4;}blockquote{border-color:rgba(2,116,190,0.15);}p,.entry-content p{margin-bottom:2.61em;}.site-title{font-size:35px;font-size:2.3333333333333rem;}.ast-archive-description .ast-archive-title{font-size:40px;font-size:2.6666666666667rem;}.site-header .site-description{font-size:15px;font-size:1rem;}.entry-title{font-size:40px;font-size:2.6666666666667rem;}.comment-reply-title{font-size:24px;font-size:1.6rem;}.ast-comment-list #cancel-comment-reply-link{font-size:15px;font-size:1rem;}h1,.entry-content h1{font-size:40px;font-size:2.6666666666667rem;}h2,.entry-content h2{font-size:30px;font-size:2rem;}h3,.entry-content h3{font-size:25px;font-size:1.6666666666667rem;}h4,.entry-content h4{font-size:20px;font-size:1.3333333333333rem;}h5,.entry-content h5{font-size:18px;font-size:1.2rem;}h6,.entry-content h6{font-size:15px;font-size:1rem;}.ast-single-post .entry-title,.page-title{font-size:30px;font-size:2rem;}#secondary,#secondary button,#secondary input,#secondary select,#secondary textarea{font-size:15px;font-size:1rem;}::selection{background-color:#0274be;color:#ffffff;}body,h1,.entry-title a,.entry-content h1,h2,.entry-content h2,h3,.entry-content h3,h4,.entry-content h4,h5,.entry-content h5,h6,.entry-content h6{color:#3a3a3a;}.tagcloud a:hover,.tagcloud a:focus,.tagcloud a.current-item{color:#ffffff;border-color:#0274be;background-color:#0274be;}.main-header-menu a,.ast-header-custom-item a{color:#3a3a3a;}.main-header-menu li:hover > a,.main-header-menu li:hover > .ast-menu-toggle,.main-header-menu .ast-masthead-custom-menu-items a:hover,.main-header-menu li.focus > a,.main-header-menu li.focus > .ast-menu-toggle,.main-header-menu .current-menu-item > a,.main-header-menu .current-menu-ancestor > a,.main-header-menu .current_page_item > a,.main-header-menu .current-menu-item > .ast-menu-toggle,.main-header-menu .current-menu-ancestor > .ast-menu-toggle,.main-header-menu .current_page_item > .ast-menu-toggle{color:#0274be;}input:focus,input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="reset"]:focus,input[type="search"]:focus,textarea:focus{border-color:#0274be;}input[type="radio"]:checked,input[type=reset],input[type="checkbox"]:checked,input[type="checkbox"]:hover:checked,input[type="checkbox"]:focus:checked,input[type=range]::-webkit-slider-thumb{border-color:#0274be;background-color:#0274be;box-shadow:none;}.site-footer a:hover + .post-count,.site-footer a:focus + .post-count{background:#0274be;border-color:#0274be;}.footer-adv .footer-adv-overlay{border-top-style:solid;border-top-color:#7a7a7a;}.ast-comment-meta{line-height:1.666666667;font-size:12px;font-size:0.8rem;}.single .nav-links .nav-previous,.single .nav-links .nav-next,.single .ast-author-details .author-title,.ast-comment-meta{color:#0274be;}.menu-toggle,button,.ast-button,.button,input#submit,input[type="button"],input[type="submit"],input[type="reset"]{border-radius:2px;padding:10px 40px;color:#ffffff;border-color:#0274be;background-color:#0274be;}button:focus,.menu-toggle:hover,button:hover,.ast-button:hover,.button:hover,input[type=reset]:hover,input[type=reset]:focus,input#submit:hover,input#submit:focus,input[type="button"]:hover,input[type="button"]:focus,input[type="submit"]:hover,input[type="submit"]:focus{color:#ffffff;border-color:#3a3a3a;background-color:#3a3a3a;}.entry-meta,.entry-meta *{line-height:1.45;color:#0274be;}.entry-meta a:hover,.entry-meta a:hover *,.entry-meta a:focus,.entry-meta a:focus *{color:#3a3a3a;}blockquote,blockquote a{color:#000000;}.ast-404-layout-1 .ast-404-text{font-size:200px;font-size:13.333333333333rem;}.widget-title{font-size:21px;font-size:1.4rem;color:#3a3a3a;}#cat option,.secondary .calendar_wrap thead a,.secondary .calendar_wrap thead a:visited{color:#0274be;}.secondary .calendar_wrap #today,.ast-progress-val span{background:#0274be;}.secondary a:hover + .post-count,.secondary a:focus + .post-count{background:#0274be;border-color:#0274be;}.calendar_wrap #today > a{color:#ffffff;}.ast-pagination a,.page-links .page-link,.single .post-navigation a{color:#0274be;}.ast-pagination a:hover,.ast-pagination a:focus,.ast-pagination > span:hover:not(.dots),.ast-pagination > span.current,.page-links > .page-link,.page-links .page-link:hover,.post-navigation a:hover{color:#3a3a3a;}.ast-header-break-point .ast-mobile-menu-buttons-minimal.menu-toggle{background:transparent;color:#0274be;}.ast-header-break-point .ast-mobile-menu-buttons-outline.menu-toggle{background:transparent;border:1px solid #0274be;color:#0274be;}.ast-header-break-point .ast-mobile-menu-buttons-fill.menu-toggle{background:#0274be;}@media (min-width:545px){.ast-page-builder-template .comments-area,.single.ast-page-builder-template .entry-header,.single.ast-page-builder-template .post-navigation{max-width:1240px;margin-left:auto;margin-right:auto;}}@media (max-width:768px){.ast-archive-description .ast-archive-title{font-size:40px;}.entry-title{font-size:30px;}h1,.entry-content h1{font-size:30px;}h2,.entry-content h2{font-size:25px;}h3,.entry-content h3{font-size:20px;}.ast-single-post .entry-title,.page-title{font-size:30px;}}@media (max-width:544px){.ast-archive-description .ast-archive-title{font-size:40px;}.entry-title{font-size:30px;}h1,.entry-content h1{font-size:30px;}h2,.entry-content h2{font-size:25px;}h3,.entry-content h3{font-size:20px;}.ast-single-post .entry-title,.page-title{font-size:30px;}}@media (max-width:768px){html{font-size:85.5%;}}@media (max-width:544px){html{font-size:85.5%;}}@media (min-width:769px){.ast-container{max-width:1240px;}}@font-face {font-family: "Astra";src: url( http://localhost/wordpress/wp-content/themes/astra/assets/fonts/astra.woff) format("woff"),url( http://localhost/wordpress/wp-content/themes/astra/assets/fonts/astra.ttf) format("truetype"),url( http://localhost/wordpress/wp-content/themes/astra/assets/fonts/astra.svg#astra) format("svg");font-weight: normal;font-style: normal;}@media (max-width:921px) {.main-header-bar .main-header-bar-navigation{display:none;}}.ast-desktop .main-header-menu.submenu-with-border .sub-menu,.ast-desktop .main-header-menu.submenu-with-border .children,.ast-desktop .main-header-menu.submenu-with-border .astra-full-megamenu-wrapper{border-color:#0274be;}.ast-desktop .main-header-menu.submenu-with-border .sub-menu,.ast-desktop .main-header-menu.submenu-with-border .children{border-top-width:2px;border-right-width:0px;border-left-width:0px;border-bottom-width:0px;border-style:solid;}.ast-desktop .main-header-menu.submenu-with-border .sub-menu .sub-menu,.ast-desktop .main-header-menu.submenu-with-border .children .children{top:-2px;}.ast-desktop .main-header-menu.submenu-with-border .sub-menu a,.ast-desktop .main-header-menu.submenu-with-border .children a{border-bottom-width:0px;border-style:solid;border-color:#eaeaea;}@media (min-width:769px){.main-header-menu .sub-menu li.ast-left-align-sub-menu:hover > ul,.main-header-menu .sub-menu li.ast-left-align-sub-menu.focus > ul{margin-left:-0px;}}.ast-small-footer{border-top-style:solid;border-top-width:1px;border-top-color:#7a7a7a;}.ast-small-footer-wrap{text-align:center;}@media (max-width:920px){.ast-404-layout-1 .ast-404-text{font-size:100px;font-size:6.6666666666667rem;}}
.ast-header-break-point .site-header{border-bottom-width:1px;}@media (min-width:769px){.main-header-bar{border-bottom-width:1px;}}.main-header-menu .menu-item, .main-header-bar .ast-masthead-custom-menu-items{-js-display:flex;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-moz-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-moz-box-orient:vertical;-moz-box-direction:normal;-ms-flex-direction:column;flex-direction:column;}.main-header-menu > .menu-item > a{height:100%;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center;-js-display:flex;display:flex;}.ast-primary-menu-disabled .main-header-bar .ast-masthead-custom-menu-items{flex:unset;}
@media (min-width:769px){.ast-theme-transparent-header #masthead{position:absolute;left:0;right:0;}.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header.ast-header-break-point .main-header-bar{background:none;}body.elementor-editor-active.ast-theme-transparent-header #masthead, .fl-builder-edit .ast-theme-transparent-header #masthead, body.vc_editor.ast-theme-transparent-header #masthead{z-index:0;}.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .custom-mobile-logo-link{display:none;}.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .transparent-custom-logo{display:inline-block;}.ast-theme-transparent-header .ast-above-header{background-image:none;background-color:transparent;}.ast-theme-transparent-header .ast-below-header{background-image:none;background-color:transparent;}}.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header.ast-header-break-point .main-header-menu, .ast-theme-transparent-header.ast-header-break-point .main-header-bar{background-color:#6394bf;}.ast-theme-transparent-header .main-header-bar .ast-search-menu-icon form{background-color:#6394bf;}.ast-theme-transparent-header .ast-above-header, .ast-theme-transparent-header .ast-below-header{background-color:#6394bf;}.ast-theme-transparent-header .main-header-menu, .ast-theme-transparent-header.ast-header-break-point .main-header-menu{background-color:#6d6d6d;}@media (max-width:768px){.ast-theme-transparent-header #masthead{position:absolute;left:0;right:0;}.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header.ast-header-break-point .main-header-bar{background:none;}body.elementor-editor-active.ast-theme-transparent-header #masthead, .fl-builder-edit .ast-theme-transparent-header #masthead, body.vc_editor.ast-theme-transparent-header #masthead{z-index:0;}.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .custom-mobile-logo-link{display:none;}.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .transparent-custom-logo{display:inline-block;}.ast-theme-transparent-header .ast-above-header{background-image:none;background-color:transparent;}.ast-theme-transparent-header .ast-below-header{background-image:none;background-color:transparent;}}.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header .site-header{border-bottom-width:0;}
.ast-breadcrumbs .trail-browse, .ast-breadcrumbs .trail-items, .ast-breadcrumbs .trail-items li{display:inline-block;margin:0;padding:0;border:none;background:inherit;text-indent:0;}.ast-breadcrumbs .trail-browse{font-size:inherit;font-style:inherit;font-weight:inherit;color:inherit;}.ast-breadcrumbs .trail-items{list-style:none;}.trail-items li::after{padding:0 0.3em;content:"»";}.trail-items li:last-of-type::after{display:none;}
</style>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel='stylesheet' id='astra-theme-css-css'  href='/astra/assets/css/minified/style.min.css?ver=1.8.1' type='text/css' media='all' />

<link rel='stylesheet' id='astra-menu-animation-css'  href='/astra/assets/css/minified/menu-animation.min.css?ver=1.8.1' type='text/css' media='all' />

<!-- <link rel='stylesheet' id='dashicons-css'  href='/css/dashicons.min.css?ver=5.1.1' type='text/css' media='all' /> -->
<link rel='stylesheet' id='wp-block-library-css'  href='/css/dist/block-library/style.min.css?ver=5.1.1' type='text/css' media='all' />

<link rel='stylesheet' id='elementor-icons-css'  href='/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=4.3.0' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css'  href='/elementor/assets/lib/animations/animations.min.css?ver=2.5.14' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='/elementor/assets/css/frontend.min.css?ver=2.5.14' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-global-css'  href='/elementor/css/global.css?ver=1590049843' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-61-css'  href='/elementor/css/post-61.css?ver=1590474530' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPT+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.1.1' type='text/css' media='all' />
<!--[if IE]>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/themes/astra/assets/js/minified/flexibility.min.js?ver=1.8.1'></script>
<script type='text/javascript'>
flexibility(document.documentElement);
</script>
<![endif]-->
<!-- <script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/easy-video-player/lib/flowplayer.min.js?ver=5.1.1'></script> -->
<script type='text/javascript' src='/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<!--[if lt IE 8]>
<script type='text/javascript' src='http://localhost/wordpress/wp-includes/js/json2.min.js?ver=2015-05-03'></script>
<![endif]-->
<!-- <script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/wp-file-upload/js/wordpress_file_upload_functions.js?ver=5.1.1'></script> -->
<script type='text/javascript' src='/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<!-- <script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/wp-file-upload/vendor/jquery/jquery-ui-timepicker-addon.min.js?ver=5.1.1'></script> -->
<!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
<!-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel='https://api.w.org/' href='http://localhost/wordpress/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/wordpress/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/wordpress/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.1.1" />
<link rel="canonical" href="http://localhost/wordpress/" />
<link rel='shortlink' href='http://localhost/wordpress/' />
<link rel="alternate" type="application/json+oembed" href="http://localhost/wordpress/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost%2Fwordpress%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://localhost/wordpress/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost%2Fwordpress%2F&#038;format=xml" />
<!-- This content is generated with the Easy Video Player plugin v1.1.8 - http://noorsplugin.com/wordpress-video-plugin/ --><script>flowplayer.conf.embed = false;flowplayer.conf.keyboard = false;</script><!-- Easy Video Player plugin --></head>

<body itemtype='https://schema.org/WebPage' itemscope='itemscope' class="home page-template-default page page-id-61 user-registration-page ast-desktop ast-page-builder-template ast-no-sidebar astra-1.8.1 ast-header-custom-item-inside ast-single-post ast-inherit-site-logo-transparent elementor-default elementor-page elementor-page-61">

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    <header itemtype="https://schema.org/WPHeader" itemscope="itemscope" id="masthead" class="site-header ast-primary-submenu-animation-fade header-main-layout-1 ast-primary-menu-enabled ast-logo-title-inline ast-hide-custom-menu-mobile ast-menu-toggle-icon ast-mobile-header-inline" role="banner">
		<div class="main-header-bar-wrap">
			<div class="main-header-bar">
				<div class="ast-container">
					<div class="ast-flex main-header-container">
						<div class="site-branding">
							<div class="ast-site-identity" itemscope="itemscope" itemtype="https://schema.org/Organization">
								<div class="ast-site-title-wrap">
									<h1 class="site-title" itemprop="name">
										<a href="/" rel="home" itemprop="url" >
										<!-- <img width="50" height="50" src="/images/logo.png"  alt=""   /> -->
										HaHu
											<!-- <s>Learn From Home</s> -->
										</a>
									</h1>
								</div>		
							</div>
						</div>

				<!-- .site-branding -->
						<div class="ast-mobile-menu-buttons">
							<div class="ast-button-wrap">
								<button type="button" class="menu-toggle main-header-menu-toggle  ast-mobile-menu-buttons-minimal "  aria-controls='primary-menu' aria-expanded='false'>
									<span class="screen-reader-text">Main Menu</span>
									<span class="menu-toggle-icon"></span>
								</button>
						    </div>
					    </div>
						<div class="ast-main-header-bar-alignment">
							<div class="main-header-bar-navigation">
								<nav itemtype="https://schema.org/SiteNavigationElement" itemscope="itemscope" id="site-navigation" class="ast-flex-grow-1 navigation-accessibility" role="navigation" aria-label="Site Navigation">
									<div class="main-navigation">
										<ul id="primary-menu" class="main-header-menu ast-nav-menu ast-flex ast-justify-content-flex-end  submenu-with-border astra-menu-animation-fade ">
											<li id="menu-item-49" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-49"><a href="/" aria-current="page">Home</a></li>
											<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a href="#service">Services</a></li>
											
											@guest
												<!-- <li class="nav-item">
													<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
												</li> -->
												<li id="menu-item-100" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-100">
												<!-- <a href="/login">Login</a> -->
												<!-- <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> -->
												<a class="nav-link" href="#getStarted">Get Started</a>
												</li>
										
											@else
												<li class="nav-item dropdown">
													<!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
														{{ Auth::user()->name }} <span class="caret"></span>
													</a> -->
													<a class="dropdown-item" href="{{ route('logout') }}"
														onclick="event.preventDefault();
																		document.getElementById('logout-form').submit();">
															{{ __('Logout') }}
												</a>

													<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
														<a class="dropdown-item" href="{{ route('logout') }}"
														onclick="event.preventDefault();
																		document.getElementById('logout-form').submit();">
															{{ __('Logout') }}
														</a>

														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>
													</div>
												</li>
											@endguest
											<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a href="/chat">Q&A</a></li>
											<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a href="#contact">Contact Us</a></li>
										</ul>
									</div>
								</nav>
							</div>
						</div>			
				    </div><!-- Main Header Container -->
			    </div><!-- ast-row -->
		    </div> <!-- Main Header Bar -->
		</div> <!-- Main Header Bar Wrap -->

				
	</header><!-- #masthead -->
	
    <div id="content" class="site-content">
        <div class="ast-container">
			<div id="primary" class="content-area primary">
				<main id="main"  role="main">
					<article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-61" class="post-61 page type-page status-publish ast-article-single">
					    <header class="entry-header ast-header-without-markup"></header><!-- .entry-header -->
						<div class="entry-content clear" itemprop="text">
							<div data-elementor-type="post" data-elementor-id="61" class="elementor elementor-61" data-elementor-settings="[]">
								<div class="elementor-inner">
									<div class="elementor-section-wrap">
										<section class="elementor-element elementor-element-626915a0 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="626915a0" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
											<div class="elementor-background-overlay"></div>
												<div class="elementor-container elementor-column-gap-default">
													<div class="elementor-row">
														<div class="elementor-element elementor-element-37acecaf elementor-invisible elementor-column elementor-col-50 elementor-top-column" data-id="37acecaf" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;}">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-6f3e38f4 elementor-widget elementor-widget-heading" data-id="6f3e38f4" data-element_type="widget" data-widget_type="heading.default">
																		<div class="elementor-widget-container">
																			<p class="elementor-heading-title elementor-size-small">How to study at home</p>		</div>
																		</div>
																		<div class="elementor-element elementor-element-7e009a8e elementor-widget elementor-widget-text-editor" data-id="7e009a8e" data-element_type="widget" data-widget_type="text-editor.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-text-editor elementor-clearfix">
																					<p>
																						Worksheets, e-books and videos uploaded on this site will enable students to continue their studies in their home.The Q&amp;A session enable students to ask question.
																					</p>
																				</div>
																			</div>
																		</div>
																		<div class="elementor-element elementor-element-603d0922 elementor-align-center elementor-widget elementor-widget-button" data-id="603d0922" data-element_type="widget" data-widget_type="button.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-button-wrapper">
																					<a href="/getStarted" class="elementor-button-link elementor-button elementor-size-sm" role="button">
																						<span class="elementor-button-content-wrapper">
																							<span class="elementor-button-text">Get Started</span>
																						</span>
																					</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="elementor-element elementor-element-28c9be6 elementor-column elementor-col-50 elementor-top-column" data-id="28c9be6" data-element_type="column">
														<div class="elementor-column-wrap">
															<div class="elementor-widget-wrap">
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
										<section class="elementor-element elementor-element-4684b5dc elementor-section-content-middle elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="4684b5dc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-element elementor-element-21d1f5f8 elementor-column elementor-col-50 elementor-top-column" data-id="21d1f5f8" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-641b62ea elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="641b62ea" data-element_type="widget" data-widget_type="icon-box.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-icon-box-wrapper">
																			<div class="elementor-icon-box-icon">
																				<span class="elementor-icon elementor-animation-" >
																					<i class="fa fa-connectdevelop" aria-hidden="true"></i>
																				</span>
																			</div>
																			<div class="elementor-icon-box-content">
																				<h3 class="elementor-icon-box-title">
																					<span >Who is it for?</span>
																				</h3>
																				<p class="elementor-icon-box-description">For students who have been looking for an aiding material to continue their studies at home and also for those who want to communicate with teachers for asking questions and talk about different issues related to learning</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="elementor-element elementor-element-7dd36af5 elementor-column elementor-col-50 elementor-top-column" data-id="7dd36af5" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-18aadfdf elementor-widget elementor-widget-image" data-id="18aadfdf" data-element_type="widget" data-widget_type="image.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-image">
																			<img width="390" height="280" src="/2020/05/attractive-african-american-female-college-260nw-288955520.jpg" class="attachment-large size-large" alt="" srcset="/2020/05/attractive-african-american-female-college-260nw-288955520.jpg 390w, http://localhost/wordpress/wp-content/uploads/2020/05/attractive-african-american-female-college-260nw-288955520-300x215.jpg 300w" sizes="(max-width: 390px) 100vw, 390px" />											
																		</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
											</div>
								        </section>
										<section id="service" class="elementor-element elementor-element-68a08255 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="68a08255" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-element elementor-element-6f9717ff elementor-column elementor-col-100 elementor-top-column" data-id="6f9717ff" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-21314e53 elementor-widget elementor-widget-heading" data-id="21314e53" data-element_type="widget" data-widget_type="heading.default">
																	<div class="elementor-widget-container">
																		<h3 class="elementor-heading-title elementor-size-large">What we provide</h3>		</div>
																	</div>
																	<section class="elementor-element elementor-element-18af8b3b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="18af8b3b" data-element_type="section">
																		<div class="elementor-container elementor-column-gap-no">
																			<div class="elementor-row">
																				<div class="elementor-element elementor-element-1e23980c elementor-invisible elementor-column elementor-col-33 elementor-inner-column" data-id="1e23980c" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomInDown&quot;}">
																					<div class="elementor-column-wrap  elementor-element-populated">[]
																						<div class="elementor-widget-wrap">
																							<div class="elementor-element elementor-element-27c8ff41 elementor-widget elementor-widget-image" data-id="27c8ff41" data-element_type="widget" data-widget_type="image.default">
																								<div class="elementor-widget-container">
																									<div class="elementor-image">
																										<img width="150" height="150" src="/2020/05/images-15-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" />											</div>
																									</div>
																								</div>
																								<div class="elementor-element elementor-element-5667e63 elementor-widget elementor-widget-heading" data-id="5667e63" data-element_type="widget" data-widget_type="heading.default">
																									<div class="elementor-widget-container">
																										<h2 class="elementor-heading-title elementor-size-large">Work Sheets</h2>		
																									</div>
																								</div>
																								<div class="elementor-element elementor-element-61517eed elementor-widget elementor-widget-text-editor" data-id="61517eed" data-element_type="widget" data-widget_type="text-editor.default">
																									<div class="elementor-widget-container">
																										<div class="elementor-text-editor elementor-clearfix">
																											<p>
																											  WorkSheets will be provided by ur teachers.
																										   </p>
																										</div>
																									</div>
																								</div>
																								<div class="elementor-element elementor-element-32d3395 elementor-shape-circle elementor-widget elementor-widget-social-icons" data-id="32d3395" data-element_type="widget" data-widget_type="social-icons.default">
																									<div class="elementor-widget-container">
																										<div class="elementor-social-icons-wrapper">
																											<a class="elementor-icon elementor-social-icon elementor-social-icon-facebook" href="" target="_blank">
																												<span class="elementor-screen-only">Facebook</span>
																													<i class="fa fa-facebook"></i>
																											</a>
																											<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter" href="" target="_blank">
																												<span class="elementor-screen-only">Twitter</span>
																												<i class="fa fa-twitter"></i>
																											</a>
																											<a class="elementor-icon elementor-social-icon elementor-social-icon-google-plus" href="" target="_blank">
																												<span class="elementor-screen-only">Google-plus</span>
																												<i class="fa fa-google-plus"></i>
																											</a>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="elementor-element elementor-element-61f073e5 elementor-invisible elementor-column elementor-col-33 elementor-inner-column" data-id="61f073e5" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomInDown&quot;}">
																						<div class="elementor-column-wrap  elementor-element-populated">
																							<div class="elementor-widget-wrap">
																								<div class="elementor-element elementor-element-ef0cde6 elementor-widget elementor-widget-image" data-id="ef0cde6" data-element_type="widget" data-widget_type="image.default">
																									<div class="elementor-widget-container">
																										<div class="elementor-image">
																											<img width="150" height="150" src="/2020/05/images-18-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" />											
																										</div>
																									</div>
																								</div>
																							<div class="elementor-element elementor-element-4876481d elementor-widget elementor-widget-heading" data-id="4876481d" data-element_type="widget" data-widget_type="heading.default">
																								<div class="elementor-widget-container">
																									<h2 class="elementor-heading-title elementor-size-large">Video</h2>		
																								</div>
																							</div>
																							<div class="elementor-element elementor-element-1c858f81 elementor-widget elementor-widget-text-editor" data-id="1c858f81" data-element_type="widget" data-widget_type="text-editor.default">
																								<div class="elementor-widget-container">
																									<div class="elementor-text-editor elementor-clearfix">
																										<p>
																											Different subjects will have videos that enables u to understand fully and also that helps you to ask question.
																										</p>
																									</div>
																								</div>
																							</div>
																							<div class="elementor-element elementor-element-6c24e553 elementor-shape-circle elementor-widget elementor-widget-social-icons" data-id="6c24e553" data-element_type="widget" data-widget_type="social-icons.default">
																								<div class="elementor-widget-container">
																									<div class="elementor-social-icons-wrapper">
																										<a class="elementor-icon elementor-social-icon elementor-social-icon-facebook" href="" target="_blank">
																											<span class="elementor-screen-only">Facebook</span>
																											<i class="fa fa-facebook"></i>
																										</a>
																										<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter" href="" target="_blank">
																											<span class="elementor-screen-only">Twitter</span>
																											<i class="fa fa-twitter"></i>
																										</a>
																										<a class="elementor-icon elementor-social-icon elementor-social-icon-google-plus" href="" target="_blank">
																											<span class="elementor-screen-only">Google-plus</span>
																											<i class="fa fa-google-plus"></i>
																										</a>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="elementor-element elementor-element-421ba646 elementor-invisible elementor-column elementor-col-33 elementor-inner-column" data-id="421ba646" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomInDown&quot;}">
																					<div class="elementor-column-wrap  elementor-element-populated">
																						<div class="elementor-widget-wrap">
																							<div class="elementor-element elementor-element-457a764f elementor-widget elementor-widget-image" data-id="457a764f" data-element_type="widget" data-widget_type="image.default">
																								<div class="elementor-widget-container">
																									<div class="elementor-image">
																										<img width="150" height="150" src="/2020/05/images-19-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" />											</div>
																									</div>
																								</div>
																								<div class="elementor-element elementor-element-73ff5b78 elementor-widget elementor-widget-heading" data-id="73ff5b78" data-element_type="widget" data-widget_type="heading.default">
																									<div class="elementor-widget-container">
																										<h2 class="elementor-heading-title elementor-size-large">E-books</h2>		
																									</div>
																								</div>
																							<div class="elementor-element elementor-element-60523770 elementor-widget elementor-widget-text-editor" data-id="60523770" data-element_type="widget" data-widget_type="text-editor.default">
																								<div class="elementor-widget-container">
																									<div class="elementor-text-editor elementor-clearfix">
																										<p>E-books will be prepared.</p>
																									</div>
																								</div>
																							</div>
																							<div class="elementor-element elementor-element-42b036c6 elementor-shape-circle elementor-widget elementor-widget-social-icons" data-id="42b036c6" data-element_type="widget" data-widget_type="social-icons.default">
																								<div class="elementor-widget-container">
																									<div class="elementor-social-icons-wrapper">
																										<a class="elementor-icon elementor-social-icon elementor-social-icon-facebook" href="" target="_blank">
																											<span class="elementor-screen-only">Facebook</span>
																												<i class="fa fa-facebook"></i>
																										</a>
																										<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter" href="" target="_blank">
																											<span class="elementor-screen-only">Twitter</span>
																											<i class="fa fa-twitter"></i>
																										</a>
																										<a class="elementor-icon elementor-social-icon elementor-social-icon-google-plus" href="" target="_blank">
																											<span class="elementor-screen-only">Google-plus</span>
																																		
																											<i class="fa fa-google-plus"></i>
																										</a>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</section>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>

										<section  id="getStarted" class="elementor-element elementor-element-b688ae8 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="b688ae8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
											<div  class="elementor-container elementor-column-gap-no">
												<div class="elementor-row">
													<div class="elementor-element elementor-element-50705782 elementor-column elementor-col-50 elementor-top-column" data-id="50705782" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-20b5b71f elementor-widget elementor-widget-image" data-id="20b5b71f" data-element_type="widget" data-widget_type="image.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-image">
																			<img width="640" height="480" src="/2020/05/images-20-1.jpeg" class="attachment-full size-full" alt="" srcset="http://localhost/wordpress/wp-content/uploads/2020/05/images-20-1.jpeg 640w, http://localhost/wordpress/wp-content/uploads/2020/05/images-20-1-300x225.jpeg 300w" sizes="(max-width: 640px) 100vw, 640px" />											</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="elementor-element elementor-element-3349c3a7 elementor-column elementor-col-50 elementor-top-column" data-id="3349c3a7" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-4ab23205 elementor-view-default elementor-widget elementor-widget-icon" data-id="4ab23205" data-element_type="widget" data-widget_type="icon.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-icon-wrapper">
																				<div class="elementor-icon">
																					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="elementor-element elementor-element-5cc2a300 elementor-widget elementor-widget-heading" data-id="5cc2a300" data-element_type="widget" data-widget_type="heading.default">
																		<div class="elementor-widget-container">
																			<h3 class="elementor-heading-title elementor-size-large">I am a teacher</h3>		
																		</div>
																	</div>
																	<div class="elementor-element elementor-element-3a521f0f elementor-widget elementor-widget-text-editor" data-id="3a521f0f" data-element_type="widget" data-widget_type="text-editor.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-text-editor elementor-clearfix">
																				<p>
																					We know about your hectic schedule. We also know the only way you truly understand a 
																					subject is by practicing it in a real environment. This is why we&#8217;ve set a playground 
																					area that&#8217;s full of hours of exercises, questions and challenges. It even has a gaming section. 
																					<div class="elementor-widget-container">
																						<div class="elementor-button-wrapper">
																							<a href="#loginn" class="elementor-button-link elementor-button elementor-size-sm" role="button">
																								Login
																							</a>
																						</div>
																					</div>
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>

											<section class="elementor-element elementor-element-3a4a4037 elementor-section-content-middle elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="3a4a4037" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
											<div class="elementor-container elementor-column-gap-no">
												<div class="elementor-row">
													<div class="elementor-element elementor-element-18d0b626 elementor-column elementor-col-50 elementor-top-column" data-id="18d0b626" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-5449832a elementor-view-default elementor-widget elementor-widget-icon" data-id="5449832a" data-element_type="widget" data-widget_type="icon.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-icon-wrapper">
																			<div class="elementor-icon">
																				<i class="fa fa-fort-awesome" aria-hidden="true"></i>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="elementor-element elementor-element-2d4218a1 elementor-widget elementor-widget-heading" data-id="2d4218a1" data-element_type="widget" data-widget_type="heading.default">
																	<div class="elementor-widget-container">
																		<h3 class="elementor-heading-title elementor-size-large">I am a student</h3>		
																	</div>
																</div>
																<div class="elementor-element elementor-element-147f5a8f elementor-widget elementor-widget-text-editor" data-id="147f5a8f" data-element_type="widget" data-widget_type="text-editor.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-text-editor elementor-clearfix">
																			<p>In addition to our worksheets,ebooks and video tutorials we also offer Q&amp;A session.</p>
																		</div>
																	</div>
																</div>
																<div class="elementor-element elementor-element-46dd3cb elementor-align-center elementor-widget elementor-widget-button" data-id="46dd3cb" data-element_type="widget" data-widget_type="button.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-button-wrapper">
																			<a href="/student" class="elementor-button-link elementor-button elementor-size-sm" role="button">
												<!-- <span > -->
																				Start Now
									<!-- </span> -->
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="elementor-element elementor-element-186b03c9 elementor-column elementor-col-50 elementor-top-column" data-id="186b03c9" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-3715fdae elementor-widget elementor-widget-image" data-id="3715fdae" data-element_type="widget" data-widget_type="image.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-image">
																			<img src="/2020/05/images-21-1.jpeg" title="images (21)" alt="images (21)" />
																		</div>
																		
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>
										 	<section id="loginn" class="elementor-element elementor-element-4684b5dc elementor-section-content-middle elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"  data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
												<div class="elementor-container elementor-column-gap-default">
													<div class="elementor-row">
														<div class="elementor-element elementor-element-21d1f5f8 elementor-column elementor-col-50 elementor-top-column" data-id="21d1f5f8" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-641b62ea elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="641b62ea" data-element_type="widget" data-widget_type="icon-box.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-icon-box-wrapper">
																				<div class="elementor-icon-box-icon">
																					<span class="elementor-icon elementor-animation-" >
																						<i class="fa fa-connectdevelop" aria-hidden="true"></i>
																					</span>
																				</div>
																				<div class="elementor-icon-box-content">
																					<h3 class="elementor-icon-box-title">
																						<span >Get Started</span>
																					</h3>
																					<p class="elementor-icon-box-description">
																						Teachers have to login before they navigate to thier page and also students have to login to attenda question and answer session that we provide.
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="elementor-element elementor-element-7dd36af5 elementor-column elementor-col-50 elementor-top-column" data-id="7dd36af5" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-18aadfdf elementor-widget elementor-widget-image" data-id="18aadfdf" data-element_type="widget" data-widget_type="image.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-image">
																			
																				<div class="container">
																					<div class="row justify-content-center">
																						<div class="col-md-8">
																							
																									<form method="POST" action="{{ route('login') }}">
																										@csrf

																										<div class="form-group row">
																											

																											<div class="col-md-11">
																												<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

																												@error('email')
																													<span class="invalid-feedback" role="alert">
																														<strong>{{ $message }}</strong>
																													</span>
																												@enderror
																											</div>
																										</div>

																										<div class="form-group row">
																											

																											<div class="col-md-11">
																												<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

																												@error('password')
																													<span class="invalid-feedback" role="alert">
																														<strong>{{ $message }}</strong>
																													</span>
																												@enderror
																											</div>
																										</div>


																										<div class="form-group row " >
																											<div class="col-md-12">
																												<button type="submit" class="btn btn-primary" id="subBtn">
																													{{ __('Login') }}
																												</button>
																												
																												@if (Route::has('password.request'))
																													<a class="btn btn-link" href="{{ route('password.request') }}">
																														{{ __('Forgot Your Password?') }}
																													</a>
																												    @endif
																												
																											</div>
																										</div>
																									</form>
																								
																						
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
											</section>
                                            
											<section id="registerr" class="elementor-element elementor-element-4684b5dc elementor-section-content-middle elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="4684b5dc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
												<div class="elementor-container elementor-column-gap-default">
													<div class="elementor-row">
														
														<div class="elementor-element elementor-element-7dd36af5 elementor-column elementor-col-50 elementor-top-column" data-id="7dd36af5" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-18aadfdf elementor-widget elementor-widget-image" data-id="18aadfdf" data-element_type="widget" data-widget_type="image.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-image">
																				<!-- <img width="390" height="280" src="http://localhost/wordpress/wp-content/uploads/2020/05/attractive-african-american-female-college-260nw-288955520.jpg" class="attachment-large size-large" alt="" srcset="http://localhost/wordpress/wp-content/uploads/2020/05/attractive-african-american-female-college-260nw-288955520.jpg 390w, http://localhost/wordpress/wp-content/uploads/2020/05/attractive-african-american-female-college-260nw-288955520-300x215.jpg 300w" sizes="(max-width: 390px) 100vw, 390px" />											</div> -->
																				<div class="container">
																					<div class="row justify-content-center">
																						<div class="col-md-8">
																							<!-- <div class="card"> -->
																								<!-- <div class="card-header">{{ __('Register') }}</div> -->

																								<!-- <div class="card-body"> -->
																									<form method="POST" action="{{ route('register') }}">
																										@csrf

																										<div class="form-group row">
																											<!-- <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label> -->

																											<div class="col-md-11">
																												<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

																												@error('name')
																													<span class="invalid-feedback" role="alert">
																														<strong>{{ $message }}</strong>
																													</span>
																												@enderror
																											</div>
																										</div>

																										<div class="form-group row">
																											<!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

																											<div class="col-md-11">
																												<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">

																												@error('email')
																													<span class="invalid-feedback" role="alert">
																														<strong>{{ $message }}</strong>
																													</span>
																												@enderror
																											</div>
																										</div>

																										<div class="form-group row">
																											<!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> -->

																											<div class="col-md-11">
																												<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

																												@error('password')
																													<span class="invalid-feedback" role="alert">
																														<strong>{{ $message }}</strong>
																													</span>
																												@enderror
																											</div>
																										</div>
																										<div class="form-group row">
																											<!-- <label for="class" class="col-md-4 col-form-label text-md-right">{{ __('class') }}</label> -->

																											<div class="col-md-11">
																											<select class="form-control" name="class">
																												<option value="HighSchool">HighSchool</option>
																												<option value="Elementory">Elementory(7-8)</option>
																												<option value="Natural">Preparatory(Natural)</option>
																												<option value="Social">Preparatory(Social)></option>
																												
																											</select>

																												@error('class')
																													<span class="invalid-feedback" role="alert">
																														<strong>{{ $message }}</strong>
																													</span>
																												@enderror
																											</div>
																										</div>

																										<div class="form-group row">
																											<!-- <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label> -->

																											<div class="col-md-11">
																												<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confrim Password">
																											</div>
																										</div>

																										<div class="form-group row mb-0">
																											<div class="col-md-6 offset-md-4">
																												<button type="submit" class="btn btn-primary">
																													{{ __('Register') }}
																												</button>
																											</div>
																										</div>
																									</form>
																								<!-- </div> -->
																							<!-- </div> -->
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div class="elementor-element elementor-element-21d1f5f8 elementor-column elementor-col-50 elementor-top-column" data-id="21d1f5f8" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-641b62ea elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="641b62ea" data-element_type="widget" data-widget_type="icon-box.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-icon-box-wrapper">
																				<div class="elementor-icon-box-icon">
																					<!-- <span class="elementor-icon elementor-animation-" >
																						<i class="fa fa-connectdevelop" aria-hidden="true"></i>
																					</span> -->
																				</div>
																				<div class="elementor-icon-box-content">
																					<h3 class="elementor-icon-box-title">
																						<span >Register</span>
																					</h3>
																					<p class="elementor-icon-box-description">
																						Students can register through this form to be able to attend on the question and answer session.
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
											</section>
											
										
											<section id="contact" class="elementor-element elementor-element-58a8eb7f elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="58a8eb7f" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;curve&quot;,&quot;shape_divider_bottom_negative&quot;:&quot;yes&quot;}">
												<div class="elementor-background-overlay"></div>
													<div class="elementor-shape elementor-shape-bottom" data-negative="true">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
															<path class="elementor-shape-fill" d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z"/>
														</svg>		
													</div>
												<div class="elementor-container elementor-column-gap-default">
													<div class="elementor-row">
														<div class="elementor-element elementor-element-21665e62 elementor-column elementor-col-100 elementor-top-column" data-id="21665e62" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-7c1eeac9 elementor-widget elementor-widget-heading" data-id="7c1eeac9" data-element_type="widget" data-widget_type="heading.default">
																		<div class="elementor-widget-container">
																			<h2 class="elementor-heading-title elementor-size-default">Contact Us</h2>		
																		</div>
																	</div>
																	<div class="elementor-element elementor-element-7b767331 elementor-widget elementor-widget-text-editor" data-id="7b767331" data-element_type="widget" data-widget_type="text-editor.default">
																		<div class="elementor-widget-container">
														
																			<div class="elementor-text-editor elementor-clearfix">
																				<p>If you want to learn more about us, you&#8217;re in the right place. Please use one of the following methods to find us.We appreciate your help and please give us our comment to improve our system and also ask any question about our system</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>
											<section class="elementor-element elementor-element-34286698 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="34286698" data-element_type="section">
												<div class="elementor-container elementor-column-gap-wide">
													<div class="elementor-row">
														<div class="elementor-element elementor-element-68a015ba elementor-column elementor-col-33 elementor-top-column" data-id="68a015ba" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-18e7dfd1 elementor-view-stacked elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="18e7dfd1" data-element_type="widget" data-widget_type="icon-box.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-icon-box-wrapper">
																				<div class="elementor-icon-box-icon">
																					<span class="elementor-icon elementor-animation-" >
																						<i class="fa fa-telegram" aria-hidden="true"></i>
																					</span>
																				</div>
																				<div class="elementor-icon-box-content">
																					<h3 class="elementor-icon-box-title">
																						<span >Telegram</span>
																					</h3>
																					<p class="elementor-icon-box-description"></p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="elementor-element elementor-element-68f32940 elementor-column elementor-col-33 elementor-top-column" data-id="68f32940" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-60ae7a63 elementor-view-stacked elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="60ae7a63" data-element_type="widget" data-widget_type="icon-box.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-icon-box-wrapper">
																				<div class="elementor-icon-box-icon">
																					<span class="elementor-icon elementor-animation-" >
																						<i class="fa fa-facebook" aria-hidden="true"></i>
																					</span>
																				</div>
																				<div class="elementor-icon-box-content">
																				    
																					<h3 class="elementor-icon-box-title">
																						<span >Faceboo</span>
																					</h3>
																					<p class="elementor-icon-box-description"></p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="elementor-element elementor-element-4c1c9005 elementor-column elementor-col-33 elementor-top-column" data-id="4c1c9005" data-element_type="column">
															<div class="elementor-column-wrap  elementor-element-populated">
																<div class="elementor-widget-wrap">
																	<div class="elementor-element elementor-element-d2fde84 elementor-view-stacked elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="d2fde84" data-element_type="widget" data-widget_type="icon-box.default">
																		<div class="elementor-widget-container">
																			<div class="elementor-icon-box-wrapper">
																				<div class="elementor-icon-box-icon">
																					<span class="elementor-icon elementor-animation-" >
																						<i class="fa fa-phone-square" aria-hidden="true"></i>
																					</span>
																				</div>
																				<div class="elementor-icon-box-content">
																					<h3 class="elementor-icon-box-title">
																						<span >Phone</span>
																					</h3>
																					<p class="elementor-icon-box-description">0911345600</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>
										</div>
									</div>
								</div>
						</div><!-- .entry-content .clear -->
					</article><!-- #post-## -->
				</main><!-- #main -->
		    </div><!-- #primary -->
		</div> <!-- ast-container -->
    </div><!-- #content -->
    <footer itemtype="https://schema.org/WPFooter" itemscope="itemscope" id="colophon" class="site-footer" role="contentinfo">
		<div class="ast-small-footer footer-sml-layout-1">
			<div class="ast-footer-overlay">
				<div class="ast-container">
					<div class="ast-small-footer-wrap" >
						<div class="ast-small-footer-section ast-small-footer-section-1" >
							Copyright &copy; 2020 <span class="ast-footer-site-title">HaHu</span> | Powered by <a href="https://wpastra.com/">Astra</a>					
						</div>
						
					</div><!-- .ast-row .ast-small-footer-wrap -->
				</div><!-- .ast-container -->
			</div><!-- .ast-footer-overlay -->
		</div><!-- .ast-small-footer-->
    </footer><!-- #colophon -->
		
		
</div><!-- #page -->

	
	<script type='text/javascript'>
/* <![CDATA[ */
var astra = {"break_point":"921","isRtl":""};
/* ]]> */
</script>
<script type='text/javascript' src='/astra/assets/js/minified/style.min.js?ver=1.8.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var themeMyLogin = {"action":"","errors":[]};
/* ]]> */
</script>
<!-- <script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/theme-my-login/assets/scripts/theme-my-login.min.js?ver=7.0.15'></script> -->
<script type='text/javascript' src='/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/js/jquery/ui/slider.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/js/wp-embed.min.js?ver=5.1.1'></script>
<script type='text/javascript' src='/elementor/assets/js/frontend-modules.min.js?ver=2.5.14'></script>
<script type='text/javascript' src='/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
<script type='text/javascript' src='/elementor/assets/lib/swiper/swiper.min.js?ver=4.4.6'></script>
<script type='text/javascript'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.5.14","urls":{"assets":"http:\/\/localhost\/wordpress\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"post":{"id":61,"title":"Home","excerpt":""}};
</script>
<script type='text/javascript' src='/elementor/assets/js/frontend.min.js?ver=2.5.14'></script>
			<script>
			/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
			</script>
			
	</body>
</html>

