@extends('layouts.app')

@section('content')
<!-- <div >
	<div class="container" >
		<div class="row">
			<div class="col-md-12 col-md-offset-2">
				<div class="panel-heading">
					Chats
				</div>
				<div class="panel-body">
				
				<chat-messages :messages="messages"></chat-messages>
				</div>
				<div >
					<chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}">
					</chat-form>
				</div>
				
			</div>
			
		</div>
	</div>
</div> -->
<div id="content" class="site-content">
        <div class="ast-container" >
			<div  class="content-area primary" id="c2">
				<main id="main" class="site-main" role="main">
					<article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-61" class="post-61 page type-page status-publish ast-article-single">
					    <header class="entry-header ast-header-without-markup"></header><!-- .entry-header -->
						<div class="entry-content clear" itemprop="text">
							<div data-elementor-type="post" data-elementor-id="61" class="elementor elementor-61" data-elementor-settings="[]">
								<div class="elementor-inner">
									<div class="elementor-section-wrap">
										
										<section id="service" class="elementor-element elementor-element-68a08255 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="68a08255" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-element elementor-element-6f9717ff elementor-column elementor-col-100 elementor-top-column" data-id="6f9717ff" data-element_type="column">
														<div class="elementor-column-wrap  elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-21314e53 elementor-widget elementor-widget-heading" data-id="21314e53" data-element_type="widget" data-widget_type="heading.default">
																	<div class="elementor-widget-container">
																		<h3 class="elementor-heading-title elementor-size-large">Join Our Discussion</h3>		</div>
																	</div>
																	<section id="c2" class="elementor-element elementor-element-18af8b3b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="18af8b3b" data-element_type="section">
																		<div class="elementor-container elementor-column-gap-no">
																			<div class="elementor-row">
																				<div class="elementor-element elementor-element-1e23980c  elementor-column elementor-col-100 elementor-inner-column" data-id="1e23980c" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomInDown&quot;}">
																					<div class="elementor-column-wrap  elementor-element-populated">
																						<div class="elementor-widget-wrap">
																							<div class="elementor-element elementor-element-27c8ff41 elementor-widget elementor-widget-image" data-id="27c8ff41" data-element_type="widget" data-widget_type="image.default">
																								<!-- <div class="elementor-widget-container">
																									<div class="elementor-image">
																										<img width="150" height="150" src="/2020/05/images-15-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" />											</div>
																									</div>
																								</div> -->
																								<div class="elementor-element elementor-element-5667e63 elementor-widget elementor-widget-heading" data-id="5667e63" data-element_type="widget" data-widget_type="heading.default">
																									<div class="elementor-widget-container panel-heading">
																										<h2 class="elementor-heading-title elementor-size-large">Chat</h2>		
																									</div>
																								</div>
																								<div class="elementor-element elementor-element-61517eed elementor-widget elementor-widget-text-editor" data-id="61517eed" data-element_type="widget" data-widget_type="text-editor.default">
																									<div class="elementor-widget-container panel-body">
																									    <chat-messages :messages="messages"></chat-messages>
																									</div>
																								</div>
																								<div class="elementor-element elementor-element-61517eed elementor-widget elementor-widget-text-editor" data-id="61517eed" data-element_type="widget" data-widget_type="text-editor.default">
																									<div class="elementor-widget-container">
																										<chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}">
																										</chat-form>
																									</div>
																								</div>
																								<!-- <div class="elementor-element elementor-element-32d3395 elementor-shape-circle elementor-widget elementor-widget-social-icons" data-id="32d3395" data-element_type="widget" data-widget_type="social-icons.default">
																									<div class="elementor-widget-container">
																										<div class="elementor-social-icons-wrapper">
																											<a class="elementor-icon elementor-social-icon elementor-social-icon-facebook" href="" target="_blank">
																												<span class="elementor-screen-only">Facebook</span>
																													<i class="fa fa-facebook"></i>
																											</a>
																											<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter" href="" target="_blank">
																												<span class="elementor-screen-only">Twitter</span>
																												<i class="fa fa-twitter"></i>
																											</a>
																											<a class="elementor-icon elementor-social-icon elementor-social-icon-google-plus" href="" target="_blank">
																												<span class="elementor-screen-only">Google-plus</span>
																												<i class="fa fa-google-plus"></i>
																											</a>
																										</div>
																									</div>
																								</div> -->
																							</div>
																						</div>
																					</div>
																	
																				</div>
																				
																			</div>
																		</div>
																	</section>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>

										

											
                                            
											
											
										
										
										</div>
									</div>
								</div>
						</div><!-- .entry-content .clear -->
					</article><!-- #post-## -->
				</main><!-- #main -->
		    </div><!-- #primary -->
		</div> <!-- ast-container -->
    </div>

@endsection