@extends('layouts.app')

@section('content')
<div id="mainContainer">
    <div class="container" id="con">
        <br/>
        <!-- <br/> -->
        <div class="row justify-content-center" >
            <div class="col-md-8">
                <div class="card">
                    <!-- <div class="card-header">{{ __('File Upload') }}</div> -->

                    <div class="card-body">
                        <form method="POST" action="/teacher/{{$user->id}}" enctype="multipart/form-data" id="fileUpload">
                            @csrf
                        
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">{{ __('File') }}</label>

                                <div class="col-md-6">
                                    <input id="" type="file" class="form-control @error('file') is-invalid @enderror" name="file" value="{{ old('file') }}" required  autofocus>

                                    @error('file')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="grade" class="col-md-4 col-form-label text-md-right">{{ __('For Grade') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="grade">
                                        @if($user->class == "HighSchool")
                                            <option value="9">10</option>
                                            <option value="10">9</option>
                                        @endif
                                        @if($user->class == "Elementory")
                                            <option value="8">8</option>
                                            <option value="7">7</option>
                                        @endif
                                        @if($user->class == "Natural")
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        @endif
                                        @if($user->class == "Social")
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        @endif

                                    </select>
                                    
                                        
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="subject" class="col-md-4 col-form-label text-md-right">{{ __('Subject') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="subject">
                                        @if($user->class == "HighSchool" || $user->class == "Elementory")
                                            <option value="English">English</option>
                                            <option value="Amharic">Amharic</option>
                                            <option value="History">History</option>
                                            <option value="Geography">Geography</option>
                                            <option value="Civics">Civics</option>
                                            <option value="Physics">Physics</option>
                                            <option value="Maths">Maths</option>
                                            <option value="Chemistry">Chemistry</option>

                                        @endif
                                        
                                        @if($user->class == "Natural")
                                            <option value="English">English</option>
                                            <option value="Amharic">Amharic</option>
                                            
                                            <option value="Civics">Civics</option>
                                            <option value="Physics">Physics</option>
                                            <option value="Maths">Maths</option>
                                            <option value="Chemistry">Chemistry</option>
                                        @endif
                                        @if($user->class == "Social")
                                            <option value="English">English</option>
                                            <option value="Amharic">Amharic</option>
                                            <option value="History">History</option>
                                            <option value="Geography">Geography</option>
                                            <option value="Civics">Civics</option>
                                            
                                            <option value="Maths">Maths</option>
                                            
                                        @endif

                                    </select>
                                    
                                        
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Catagory" class="col-md-4 col-form-label text-md-right">Catagory</label>
                                
                                <select  name="Catagory" class="col-md-6 form-control">
                                    <option value="Video tutorial">Video Tutorial</option>
                                    <option value="tutorials">Tutorial</option>
                                    <option value="WorkSheets">WorkSheets</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                    <textarea name="description" class="form-control"></textarea>

                                    <!-- @error('file')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror -->
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Upload') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--      -->
<div>
    <div class="row justify-content-center" id="underContainer">
        <div class="col-md-4 rowDiv2">
           <div class="card">
                <div class="card-header">
                    Videos
                </div>
                <div class="card-body">
                    <div class="row">
                        @if($videosNum > 0)
                            @foreach($videos as $video)
                                <div class="col-md-11 rowDiv" >
                                    <div class="card">
                                        <div class="card-header"><img src="{{ asset('images/video.png') }}" width="100" height="100" class="center"></div>                    
                                    
                                        <div class="card-body" class="material">
                                            <b>Subject</b>-{{$video->subject}}<br/>
                                            <b> Grade</b>-{{$video->grade}}<br/>
                                            <b> Description</b>-{{$video->description}}<br/>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h3>You didnot upload any videos</h3>
                        @endif
                    </div>
                    <!-- <video src="{{ asset('Materials/Laravel 5.2 How to display image from database on laravel 5.2 part 8.mp4') }}" width="200" height="200">
                    </video> -->
                </div>
            </div>
        </div>
        <div class="col-md-4 rowDiv2">
           <div class="card">
                <div class="card-header">
                    Pdf
                </div>
                <div class="card-body">
                    <div class="row">
                        @if($pdfNum > 0)
                            @foreach($pdfs as $pdf)
                                <div class="col-md-11 rowDiv" >
                
                                    <div class="card">
                                        <div class="card-header" ><img src="{{ asset('images/pdf.png') }}" width="100" height="100" class="center"></div>                    
                                
                                    <div class="card-body" class="material">
                                        <b>Subject</b>-{{$pdf->subject}}<br/>
                                        <b> Grade</b>-{{$pdf->grade}}<br/>
                                        <b> Description</b>-{{$pdf->description}}<br/>
                                        <a class="gradeLink" href="/student/view/pdf/{{ $pdf->id }}">Open Pdf</a>
                                    </div>
                                </div>
                            
                            </div>
                            @endforeach
                        @else
                            <h3>You didnot upload any tutorials</h3>
                        @endif
                    </div>
                    <!-- <video src="{{ asset('Materials/Laravel 5.2 How to display image from database on laravel 5.2 part 8.mp4') }}" width="200" height="200">
                    </video> -->
                    <!-- <iframe src="{{ asset('Materials/1590765298.pdf') }}" width="100" height="100"></iframe>  -->
                </div>
            </div>
        </div>
        <div class="col-md-4 rowDiv2">

            <div class="card">
                <div class="card-header">
                    WorkSheets
                </div>
                <div class="card-body">
                    @if($sheetsNum > 0)
                        <h3>there are worksheets</h3>
                    @else
                        <h3>You didnot upload any worksheets</h3>
                    @endif
                    <!-- <video src="{{ asset('Materials/Laravel 5.2 How to display image from database on laravel 5.2 part 8.mp4') }}" width="200" height="200">
                    </video> -->
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

