@extends('layouts.app')

@section('content')

<div class="container">

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter no" class="form-control col-md-4">
     </div>

</div>
<div class="header" id="staffContainer" >
<div class="row">

   
    @if(count($notifiedMaterials) >= 1)

        @foreach($notifiedMaterials as $material)
          
                <div class="card col-md-4">
                	@if($material->type == 'tutorials' )
                     <div class="card-header" ><img src="{{ asset('images/pdf.png') }}" width="100" height="100" class="center"></div>                    
                    @endif
                    @if($material->type == 'Video tutorial' )
                      <div class="card-header"><img src="{{ asset('images/video.png') }}" width="100" height="100" class="center"></div> 
                    @endif
                
                    <div class="card-body">
                    	<div >
                    	 Uploaded By:Teacher with id {{ $material->teacherId }}<br /><br />
                    	 <b>Subject</b>-{{$material->subject}}<br/>
                         <b> Grade</b>-{{$material->grade}}<br/>
                       <b> Description</b>-{{$material->description}}<br/>
                       @if($material->type == 'tutorials' )
                          <a class="gradeLink2" href="/student/view/pdf/{{ $material->id }}">Open {{ $material->type }}</a>
                          <a class="pull-right">Mark as Read</a>
                        @endif
                        @if($material->type == 'Video tutorial' )
                          <a class="gradeLink2" href="/student/view/video/{{ $material->id }}">play</a>
                          <a class="pull-right" href="/notification/read/{{ $material->notificationId}}">Mark as Read</a>
                        @endif

                        
                       
                    </div>  
                </div>
        <!-- </form> -->
        @endforeach
    @else
        <h1 id="empty">You donot have new notification</h1>

    @endif
  </div>
 </div>

@endsection