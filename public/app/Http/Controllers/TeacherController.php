<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\material;
use App\User;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        
        $user = Auth::user();
        // if()
        if($user->type == "Teacher")
        {
          $materials=material::all()->where('uploadedBy','=',$user->id);
          //
          $videos=$materials->where('type','=','Video tutorial');
          $pdfs=$materials->where('type','=','tutorials');
          $sheets=$materials->where('type','=','WorkSheets');
          $videosNum=count($videos);
          $pdfNum=count($pdfs);
          $sheetsNum=count($sheets);
          // return count($videos);
          //return $pdfs;
          

          $id=$user->id;
          // return $id;
          return view('teacher',compact('user','videos','pdfs','sheets','videosNum','pdfNum','sheetsNum'));
        }
        else{
          return "u are not allowed to access this route";
        }
    }

    public function upload(Request $request,$id)
    {
        $user = Auth::user();
        
        // $attribute=request()->validate([
        //     'image' => 'required|file',
        // ]);
        if($user->type == "Teacher")
        {
          $file=$request->file('file');
          $extension=$file->getClientOriginalExtension();
          $fileName=time().'.'.$extension;

          //$fileName = time().'.'.request()->file->getClientOriginalExtension();
        
          request()->file->move(public_path('Materials'), $fileName);
          material::create([
            'uploadedBy' => $id,
            'fileName'=>$fileName,
            'fileType'=>$extension,
            'grade' =>request('grade'),
            'subject'=>request('subject'),
            'type'=>request('Catagory'),
            'description'=>request('description')
          ]);

          $materials=material::all()->where('uploadedBy','=',$user->id);
          //
          $videos=$materials->where('type','=','Video tutorial');
          $pdfs=$materials->where('type','=','tutorial');
          $sheets=$materials->where('type','=','WorkSheets');
          $videosNum=count($videos);
          $pdfNum=count($pdfs);
          $sheetsNum=count($sheets);

          return view('teacher',compact('user','videos','pdfs','sheets','videosNum','pdfNum','sheetsNum'));
        }

        
        
    }
    
    
}
