<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\material;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->type == "Student")
        {
            $materials=material::all();
            $length=count($materials);
            return view('student',compact('materials','length'));
        }
        else{
            $materials=material::all()->where('uploadedBy','=',$user->id);
          //
            $videos=$materials->where('type','=','Video tutorial');
            $pdfs=$materials->where('type','=','tutorials');
            $sheets=$materials->where('type','=','WorkSheets');
            $videosNum=count($videos);
            $pdfNum=count($pdfs);
            $sheetsNum=count($sheets);
            // return count($videos);
            //return $pdfs;
            

            $id=$user->id;
            // return $id;
            return view('teacher',compact('user','videos','pdfs','sheets','videosNum','pdfNum','sheetsNum'));
        }
        return view('welcome');
    }
}
