<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\material;
use App\User;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function view()
    {
      return view('teacherRegistration');
    }
    public function register(Request $request)
    {
      $user=User::create([
        'name' => request('name'),
        'email' => request('email'),
        'password' => Hash::make(request('password')),
        'class'=>request('class'),
        'type'=>'Teacher'

      ]);
      $materials=material::all()->where('uploadedBy','=',$user->id);
      //
      $videos=$materials->where('type','=','Video tutorial');
      $pdfs=$materials->where('type','=','tutorial');
      $sheets=$materials->where('type','=','WorkSheets');
      $videosNum=count($videos);
      $pdfNum=count($pdfs);
      $sheetsNum=count($sheets);

      return view('teacher',compact('user','videos','pdfs','sheets','videosNum','pdfNum','sheetsNum'));
    }
    public function index()
    {
        $materials=material::all();
        $length=count($materials);
        return view('student',compact('materials','length'));
    }
    public function grade($id)
    {
        $materials=material::all()->where('grade','=',$id);
        $length=count($materials);
        return view('student',compact('materials','length'));
    }
    public function Subject($id,$subject)
    {
        $materialBasedGrade=material::all()->where('grade','=',$id);
        $materials=$materialBasedGrade->where('subject','=',$subject);
        $length=count($materials);
        return view('student',compact('materials','length'));
    }
    public function viewPdf($id)
    {
        $material=material::find($id);
        return view('viewPdf',compact('material'));

        
        
    }
    public function viewVideo($id)
    {
        $material=material::find($id);
        $fileName=$material->fileName;
        return view('viewVideo',compact('fileName'));

        
        
    }
    
}
