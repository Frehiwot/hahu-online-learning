<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class material extends Model
{
    protected $fillable=['uploadedBy','fileName','fileType','description','grade','subject','type'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
