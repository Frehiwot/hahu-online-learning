<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class notificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function get()
    {
        $notification=Auth::user()->unreadNotifications;
        $notifiedMaterials=[];
        $i=0;
        $j=0;
        $uploader=[];
        foreach($notification as $notifies)
        {
            $material=\App\material::find($notifies->data['material']);
            $notifiedMaterials[$i]=$material;
            $notifiedMaterials[$i]->teacherId=$notifies->data['teacher'];
            $notifiedMaterials[$i]->notificationId=$notifies->id;
            $i++;
            // $i++;
            // $approvedMaterial=$uploadedTime->where('approved','=',0);
        }
        // return view('admin',compact('notification'));
        // echo 'hello';
        return view('admin',compact('notifiedMaterials'));
        
    }
    public function read($id)
    {
       Auth::user()->unreadNotifications()->find($id)->markAsRead();
       $notification=Auth::user()->unreadNotifications;
        $notifiedMaterials=[];
        $i=0;
        $j=0;
        $uploader=[];
        foreach($notification as $notifies)
        {
            $material=\App\material::find($notifies->data['material']);
            $notifiedMaterials[$i]=$material;
            $notifiedMaterials[$i]->teacherId=$notifies->data['teacher'];
            $notifiedMaterials[$i]->notificationId=$notifies->id;
            $i++;
            // $i++;
            // $approvedMaterial=$uploadedTime->where('approved','=',0);
        }
        // return view('admin',compact('notification'));
        // echo 'hello';
        return view('admin',compact('notifiedMaterials'));
       
    }
}
