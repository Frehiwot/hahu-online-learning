<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class notify extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $admin;
    public $teacher;
    public $material;
    public function __construct($admin,$teacher,$material)
    {
        $this->admin=$admin;
        $this->teacher=$teacher;
        $this->material=$material;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    // public function toBroadcast($notifiable)
    // {
    //      return [
             
    //             'sid' => $this->sid,
    //             'rid' => $this->rid,
    //             'type' => $this->type
             
    //          ];
    // }
    public function toArray($notifiable)
    {
        return [
            
            'admin' => $this->admin,
            'teacher' => $this->teacher,
            'material' => $this->material
        ];
    }
}
